# Changelog

## 0.3.0 (2024-10-08)

 - Add type hints
 - Add 'typing_extensions' dependency
 - Bump minimum required Python version to 3.10

## 0.2.0 (2024-02-03)

 - Mark host and port parameters as optional
 - Add parameter to specify connection and socket timeout
 - Add support for break- and watchpoints

## 0.1.2 (2023-11-05)

- Fix result parsing in targets() and target_types()
- Add additional keywords and repository URL in setup.py

## 0.1.1 (2022-05-28)

- Add CHANGELOG.md file.
- pyproject.toml: Add missing dependency.
- Fix wrong path in Flake8 configuration.

## 0.1.0 (2022-05-28)

- Initial release.
